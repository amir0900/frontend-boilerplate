const Path = require('path');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
module.exports = {
  entry: [
    Path.resolve(__dirname, './polyfills'),
    Path.resolve(__dirname, '../src/scripts/main.ts')
  ],
  output: {
    path: Path.join(__dirname, '../build'),
    filename: 'js/[name].js'
  },
  plugins: [

    new CleanWebpackPlugin(),
    new HtmlWebpackPlugin({
      template: Path.resolve(__dirname, '../src/index.html')
    })
  ],
  resolve: {
    alias: {
      '~': Path.resolve(__dirname, '../src')
    },
    extensions: ['.ts', '.js', '.json']
  },
  module: {
    rules: [
      {
        test: /\.mjs$/,
        include: /node_modules/,
        type: 'javascript/auto'
      },
       /****************
     * PRE-LOADERS
     *****************/
    {
      enforce: 'pre',
      test: /\.js$/,
      use: 'source-map-loader'
    },
    {
      enforce: 'pre',
      test: /\.ts$/,
      exclude: /node_modules/,
      use: 'tslint-loader'
    },

    /****************
    * LOADERS
    *****************/
    {
      test: /\.ts$/,
      exclude: [/node_modules/],
      use: 'awesome-typescript-loader'
    },
      {
        test: /\.(ico|jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2)(\?.*)?$/,
        use: {
          loader: 'file-loader',
          options: {
            name: '[path][name].[ext]'
          }
        }
      }
    ]
  }
};
